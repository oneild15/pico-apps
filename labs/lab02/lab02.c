#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.


/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

float wallis(float n){//the wallis function of approximating pi, using floating point variables
  float x;
  x = ((2*n)/(2*n - 1)) * ((2*n)/(2*n + 1));
  return x;  
}

double dwallis(double n){//the wallis function of approximating pi, using double variables
  double x;
  x = ((2*n)/(2*n - 1)) * ((2*n)/(2*n + 1));
  return x;  
}

int main() {

  //declaring variables
  float approx;//floating point approximation of pi
  int maxi = 100000;//maximum number of iterations required
  float add;//variable used for each iteration of the wallis function
  float ferror;//error of the floating point approximation
  double dadd;//used for each iteration of the wallis function
  double derror;//error of the double approximation of pi
  double dapprox;//double approximation of pi
  
  approx = wallis(1);//beginning the approximation of pi

  for(int i=2;i<maxi+2;i++){//for loop to iterate the wallis function-float
    add = wallis(i);
    approx = approx * add;
  }
  approx = approx*2;//wallis function requires the result being multiplies by 2 to get pi
  ferror = ((approx - 3.14159265359)/3.14159265359) * 100;//percent error of result
  
  dapprox = dwallis(1);//beinning approximation of pi

  for(int i=2;i<maxi+2;i++){//for loop iterating the value of pi-double
    dadd = dwallis(i);
    dapprox = dapprox * dadd;
  }
  dapprox = dapprox*2;
  derror = ((dapprox - 3.14159265359)/3.14159265359) * 100;//percent error of result

  //printing results to consol
  printf("Approximate float value of pi is %f \n",approx);
  printf("Error of float value is %f percent \n", ferror);
  printf("Approximate double value of pi is %f \n",dapprox);
  printf("Error of double value is %f percent \n", derror);

  return 0;
}

